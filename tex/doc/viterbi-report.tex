\documentclass[12pt]{article}

\usepackage{setspace} % Required to manage line spaces
\setlength{\parskip}{1.3em}
\usepackage{amsmath}	% Math functions
\usepackage{graphicx} % Required to insert images
\usepackage{float} % Provides positioning for images
\usepackage{color} % Required to use color
\usepackage{subcaption} % Required for double images
\definecolor{mypink}{rgb}{0.8,0.3,0.8}
\definecolor{mygreen}{rgb}{0,0.5,0}
\definecolor{myblue}{rgb}{0,0,0.5}
\definecolor{mygray}{rgb}{0.6,0.6,0.6}
\usepackage{listings} % Required to insert formated code
\lstset{language=VHDL,
        tabsize=2,
        basicstyle=\scriptsize\ttfamily,
        numbers=left,
        keywordstyle=\color{myblue},
        morekeywords={integer, time, ns, work}, % TODO: Verify keywords in other files
        stringstyle=\color{mygray},
        morestring=[b]",
        identifierstyle=\color{black},
        numberstyle=\color{mygray},
        emph=[1]{IEEE, STD_LOGIC_1164, numeric_std, std_logic, std_logic_vector, to_unsigned, rising_edge}, % Verify pink words
        emphstyle=[1]{\color{mypink}},
        commentstyle=\color{mygreen}}

\usepackage[backend=biber, bibstyle=ieee, citestyle=alphabetic]{biblatex} % Required to insert the bib file with bibliography
\bibliography{viterbi-report}

\begin{document}

\begin{titlepage}
  \newcommand{\HRule}{\rule{\linewidth}{1pt}} % Defines a new command for the horizontal lines, change thickness here
  \center % Center everything on the page

  %	HEADING SECTIONS
  {\LARGE Dortmund University of Applied Sciences and Arts}\\[2cm] % Name of your university/college
  {\large Master Embedded Systems for Mechatronics}\\[0.6cm] % Major heading such as course name
  {\large Microelectronics HW-SW Codesign}\\[0.6cm] % Minor heading such as course title

  %	TITLE SECTION
  {
  	\HRule \\[0.4cm]
  	\huge \bfseries Convolutional encoding with Viterbi decoding in VHDL\\
  	\HRule \\[0.5cm]
  }

  %	AUTHOR SECTION
  \begin{minipage}{0.4\textwidth}
  \begin{flushleft} \large
  \emph{Authors:}\\
  Javier Reyes\\
  Anna Kosenkova\\
  Mysara Ibrahim\\
  \end{flushleft}
  \end{minipage}
  ~
  \begin{minipage}{0.4\textwidth}
  \begin{flushright} \large
  \emph{Supervisor:} \\
  Prof. Dr.-Ing. Peter Schulz % Supervisor's Name
  \end{flushright}
  \end{minipage}\\[1cm]

  %	DATE SECTION
  {\large \today}\\ % Date, change the \today to a set date if you want to be precise

  %	LOGO SECTION
  \includegraphics[scale=0.3]{img/fh-logo.png}\\ % Include a department/university logo - this will require the graphicx package

\end{titlepage}

\tableofcontents
\newpage

\section{Introduction}	% The * makes the section not to be numbered

% BUG: Cite with WON09 has a plus sign
Communication in digital environments faces several challenges in terms of reliability and efficiency of data transmission over noisy channels, as errors can be introduced in a message transmission. To overcome this issue, Forward Error Correction (FEC) has been a powerful tool by introducing redundancy into the information stream\cite{WER10}\cite{WON09} to the source data transmision. Compared to Error Detection, the cmplexity of a FEC technique is more complex, but also avoids implementations for Automatic Repeat Request (ARQ) when the message received has been labeled as corrupted.

Some of the digital communication systems rely on the use of Convolutional Code encoding with Viterbi decoding to compensate for Additive White Gaussian Noise (AWGN), due to its simplicity and low-cost implementation. In terms of the decoding process, is also notable that the decoding time is fixed\cite{RAJ14}, making this technique applicable for hardware implementations.

% TODO: Insert encoder decoder diagram

As an alternative to traditional software implementations with limitations of time requirements for digital data transmission, Field Programmable Gate Array (FPGA) suppose an improvement as higher speeds can be achieved. The complexity of a Viterbi encoder grows exponentially with the increase of the number of states of the convolutional encoder\cite{SHA09}, which relates to the constraint length of the encoder.

Here we present an implementation of a Convolutional encoder and a Viterbi decoder in VHDL language, for simulations purposes only. Further work can be made by the implementation of this hardware modules on an FPGA platform and perform HiL techniques to measure the real performance.

\subsection{Code parameters}

The parameters that define a convolutional code encoder\cite{LAN99}\cite{RAJ14} can be summarized as follows:

\begin{itemize}
  \item Encoder
  \begin{itemize}
    \item Code rate $R=\frac{k}{n}$, with $k=input bits$ and $n=output bits$
    \item Constraint length $K$ (also called $L$), where $K=k(m-1)$ and $m=memory registers$
  \end{itemize}
  \item Decoder
  \begin{itemize}
    \item Hard-/soft decision decoding (if the decoder is quantized to two levels, it is hard-decision)
  \end{itemize}
\end{itemize}

The parameters mentioned above for a Viterbi implementation ($n$, $k$, $m$) are defined as global constants for the entire implementation, using a package with the values for all constants, and other relevant global definitions:

% Configuration package
\lstinputlisting[ firstline=12,
                  lastline=29,
                  firstnumber=12,
                  caption=Configuration package]{../conf_pkg.vhd}

This constant values are imported into every single module where they are required, with an import sequence:

\begin{lstlisting}[numbers=none]
  use work.conf_pkg.all;
\end{lstlisting}

As an example, we are defining a convolutional code with a code rate of $\frac{1}{2}$ and a constraint length of $3$.

\section{Convolutional Code encoder}

A convolutional encoder behaves as a Finite State Machine (FSM)\cite{RAJ14}, converting the single input bit into two or more bits. Each output bit is generated by a modulo-2 adder (XOR) of specified bits from a shift register where the input stream is applied on one end and is shifted out on the other end, producing a different state at every clock cycle.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.4]{img/states-code.png}
  \caption{States of the code}
  \label{fig:st}
\end{figure}

The selection of bits that are connected to the adder of an output is defined with a so called Generator Polynomial of that output. This gives the encoder its unique error protection quality. There is no clear consensus about the selection of the polynomials, but some recommendations based on simulation studies can be found on \cite{LAN99}.

The state of the encoder can be imagined as an initial condition, that influences the coded output. The number of states is defined by the number of registers as $states=2^{registers}$. To represent the states its behavior, several techniques can be used, as shown if figure \ref{fig:stdiags}.

\begin{figure}[H]
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.4]{img/states-diagram.png}
    \caption{States diagram}
    \label{fig:stdiag}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[scale=0.3]{img/trellis-diagram.png}
    \caption{Trellis diagram}
    \label{fig:trel}
  \end{subfigure}
  \caption{Reresentation of the encoder behavior}
  \label{fig:stdiags}
\end{figure}

The encoder module structure starts with the definition of input ports, output ports and internal components, in accordance with the parameters for the technological implementation.

\subsection{Encoder components}

The encoder is built in a modular manner, with a shift register module and a modulo-2 adders that include a generator polynomial for each output bit.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.4]{img/encoder-schematic.png}
  \caption{Encoder schematic}
  \label{fig:encschem}
\end{figure}

\subsubsection{Shift register module}

The shift register module is a unique component in the design. Its entity is based on the required input and output elements, parametrized by the values in the configuration package:

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.4]{img/shiftreg-schematic.png}
  \caption{Shift register schematic}
  \label{fig:srschem}
\end{figure}

% Entity
\lstinputlisting[firstline=14, lastline=20, firstnumber=14, caption=Shift register entity]{../shift_reg.vhd}

The shift register module is easily defined as a procedural model \cite[p. 192]{KAE08} with a single process block that will be triggered with every clock cycle, or by an asynchronous reset signal:

% Architecture
\lstinputlisting[firstline=22, lastline=43, firstnumber=22, caption=Shift register architecture]{../shift_reg.vhd}

The signal \lstinline{data} models the registers that save the bits consecutively. The output is taken directly from this registers as a structural assignation. The process block performs a shift in the register bits every rising edge in the clock signal in the direction from the MSB to the LSB.

\subsubsection{Shift register validation}

To validate the behavior of the module, input and output signals are created in the testbench matching the sizes and types of the module tested:

% Signals
\lstinputlisting[firstline=22, lastline=28, firstnumber=22, caption=Shift register testbench signals]{../shift_reg_tb.vhd}

The shift register block is instantiated as a component, in the same manner as in the encoder module:

% Component
\lstinputlisting[firstline=30, lastline=36, firstnumber=30, caption=Shift register instance in testbench]{../shift_reg_tb.vhd}

The behavior is implemented with two concurrent process blocks (one for the clock signal and the other one for the input bits):

% Clock process
\lstinputlisting[firstline=38, lastline=50, firstnumber=38, caption=Shift register testbench clock signal]{../shift_reg_tb.vhd}

% Sequence input
\lstinputlisting[firstline=52, lastline=81, firstnumber=52, caption=Shift register testbench input signal]{../shift_reg_tb.vhd}

The module is tested as a single module with the defined testbench file, in a VHDL simulation tool\footnote{Xilinx Vivado Design Suite, in the WebPack Edition available on the web page.}. The results are shown in figure \ref{fig:srsim}:

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.4]{img/shiftreg-sim.png}
  \caption{Shift register simulation}
  \label{fig:srsim}
\end{figure}

\subsubsection{Modulo-2 adder module}

The adder module is based on a modulo-2 entity, with sizes according to the parameters of the encoder:

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.4]{img/adder-schematic.png}
  \caption{Modulo-2 adder schematic}
  \label{fig:addschem}
\end{figure}

% Entity
\lstinputlisting[firstline=14, lastline=17, firstnumber=14, caption=Adder entity]{../adder.vhd}

The behavior is slightly different from a standard adder, as the inputs are first masked against the generator plynomial, and then the bits are summed up. The output is updated when the input changes:

% Architecture
\lstinputlisting[firstline=21, lastline=41, firstnumber=21, caption=Adder architecture]{../adder.vhd}

\subsubsection{Modulo-2 adder validation}

A testbench performs the validation of the adder module, following the structure and methodology of the shift register testbench:

% Signals
\lstinputlisting[firstline=18, lastline=32, firstnumber=18, caption=Adder testbench signals and instances]{../adder_tb.vhd}

A well-known value is fed to the input, to validate the correct calculation:

% Process
\lstinputlisting[firstline=34, lastline=65, firstnumber=34, caption=Adder testbench behavior]{../adder_tb.vhd}

The results of the simulation are shown in figure \ref{fig:addsim}:

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.4]{img/adder-sim.png}
  \caption{Modulo-2 adder simulation}
  \label{fig:addsim}
\end{figure}

\subsection{Encoder module}

The encoder entity is designed based on the defined parameters, as a $(2, 1, 4)$ Convolutional Code:

% Entity
\lstinputlisting[firstline=18, lastline=23, firstnumber=18, caption=Encoder entity]{../encoder.vhd}

The architecture of the encoder module requires the instances of the internal components.

% Components
\lstinputlisting[firstline=25, lastline=39, firstnumber=25, caption=Encoder architecture]{../encoder.vhd}

Once the components are available, signals need to be defined to interconnect the inputs, components and outputs.

% Architecture
\lstinputlisting[firstline=41, lastline=50, firstnumber=41, caption=Encoder architecture]{../encoder.vhd}

\subsection{Encoder validation}

Once all the components of the encoder have been implemented and tested, the entire encoder can be validated:

% Signals
\lstinputlisting[firstline=23, lastline=42, firstnumber=23, caption=Encoder tesbench signals and instances]{../encoder_tb.vhd}

% Process
\lstinputlisting[firstline=44, lastline=73, firstnumber=44, caption=Encoder testbench behavior]{../encoder_tb.vhd}

The input sequence is followed by flush bits so that the encoder processes all its possible states. The results are shown in the figure \ref{fig:encsim}:

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.5]{img/encoder-sim.png}
  \caption{Encoder simulation}
  \label{fig:addsim}
\end{figure}

\section{Viterbi decoder}

Here...
% TODO: Complete chapter (recursively)

\subsection{Structure of the decoder}

\subsection{Decoder interface}

\subsubsection{Branch Metric Unit}
\subsubsection{Add Compare Select Unit}
\subsubsection{Survivor Memory Management Unit}

\subsection{Decoder validation}

\section{Top level validation}

\begin{appendix}
	\newpage
	\listoffigures % Insert a list of figures
	\newpage
	\listoftables % Insert a list of tables
\end{appendix}

\newpage
\printbibliography % Insert the bibliography in viertbi-report.bib (all)
\nocite{*}

\end{document}
