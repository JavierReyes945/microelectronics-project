# microelectronics-project
Repository for the Microelectronics HW SW Codesign lecture Project
Summer semester 2017 - Master Embedded Systems for Mechatronics - Fachhochschule Dortmund

## General considerations

This project is managed from Taiga platform. To follow the status of the project, please check:
https://tree.taiga.io/project/an2-microelectronics/ **deprecated**

For communication purposes, there is a slack channel available here:
https://microelectronicsgroup.slack.com/ **deprecated**

The theoretical base is derived from [this document](http://teacher.buet.ac.bd/yusufsarwar/CSE321N/ConvolutionCode.pdf).

## Stucture of the project

The project contains the necessary VHDL codes for you to include them in your favorite IDE, and their corresponding testbench to be simulated. It also contains a tex folder with the necessary latex files to poduce pdf document report.

The source codes developed are:
* conf_pkg.vhd
* encoder.vhd - encoder_tb.vhd
  * shift_reg.vhd - shift_reg_tb.vhd
  * polyadder.vhd - polyadder_tb.vhd
* decoder.vhd
  * branch_metric_unit.vhd
  * add_compare_select_unit.vhd
    * adder_simple.vhd
    * comparator.vhd
    * selector.vhd
* tex/
  * viterbi-report.tex: latex source for the report document
  * viterbi-report.bib: bibliography references (biblatex+biber)
  * rep-make.mk: to compile the report document correctly
  * viterbi-report.pdf: report output file
  * slides-viterbi.tex: latex source for the slides presentation
  * slid-make.mk: to compile the slides presentation correctly
  * slides-viterbi.pdf: presentation output file

## Global configuration Package

All the files should include a `use` call for the package `conf_pkg.vhd`, which contains the definitions of the sizes for the entire design.

```
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.conf_pkg.all;
```

Any other necessary global definition should also be included in the mentioned package.

## Encoder

The design of the encoder includes a shift register unit and a modulo-2 adder. Both are designed independently.

### Shift register

The shift register is size-configurable from the constants in the package. The MSB of the output is the first shifting position, and continues shifting from MSB to LSB, at every positive edge of the clock signal. The behavior has been tested in Vivado simulator, with the respective `shift_reg_tb` testbench file.

### Modulo-2 adder

The adder module is size-configurable from the constants in the package. The module reacts to any change in the input value, and present the output of the adders in the output vector, with MSB being the superior polynomial answer, and so on. The behavior has been tested in Vivado simulator, with the respective `adder_tb` testbench file.

## Decoder

> TODO: Define entity, architecture and subcomponents

## Simulation and validation

The simulation is made through testbench files to be run in any VHDL simulator. For our validation, the Vivado tool was used.

## Authors

* **Javier Reyes** - **Anna Kosenkova** - **Mysara Ibrahim**

## License

This code is licensed under the GNU [General Public License](License.md)
