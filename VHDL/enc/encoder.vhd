-- Company: Fachhochschule Dortmund
-- Engineer: Javier Reyes
--
-- Create Date: 06/16/2017 03:07:27 PM
-- Design Name: Encoder for Convolutional Codes example project
-- Module Name: encoder - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity encoder is
  port (enc_in  : in std_logic_vector(k-1 downto 0);
        enc_clk : in std_logic;
        enc_rs  : in std_logic;
        enc_out : out std_logic_vector(n-1 downto 0));
end encoder;

architecture Behavioral of encoder is

-- Components declaration
component shift_reg
  port (sr_clk  : in std_logic;
        sr_rst  : in std_logic;
        sr_in   : in std_logic_vector(k-1 downto 0);
        sr_data : out std_logic_vector(m-1 downto 0));
        -- MSB is data, LSB is data at t-3
end component shift_reg;

component polyadder
  port (add_in  : in std_logic_vector(m-1 downto 0);
        add_out : out std_logic_vector(n-1 downto 0));
end component polyadder;

-- Signals declaration
signal shift2add : std_logic_vector(m-1 downto 0);

begin

  -- instantiating the components
  SR1 : shift_reg port map (enc_clk, enc_rs, enc_in, shift2add);
  AD1 : polyadder port map (shift2add, enc_out);

end Behavioral;
