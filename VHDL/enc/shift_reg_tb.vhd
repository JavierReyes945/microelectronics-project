-- Company: Fachhochschule Dortmund
-- Engineer: Javier Reyes
--
-- Create Date: 06/16/2017 04:49:58 PM
-- Design Name: Shift register Testbench for Convolutional Codes example project
-- Module Name: shift_reg_tb - Simulation
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.conf_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity shift_reg_tb is
  -- EMPTY
end shift_reg_tb;

architecture Behavioral of shift_reg_tb is

-- Signals declaration
signal clk   : std_logic := '0';
signal rst   : std_logic := '0';
signal d_in  : std_logic_vector(k-1 downto 0) := std_logic_vector(to_unsigned(0, k));
signal d_out : std_logic_vector(m-1 downto 0);

-- Components declaration
component shift_reg
  port (sr_clk  : in std_logic;
        sr_rst  : in std_logic;
        sr_in   : in std_logic_vector(k-1 downto 0);
        sr_data : out std_logic_vector(m-1 downto 0));
end component;

begin

  -- instantiating the components
  SR : shift_reg port map (clk, rst, d_in, d_out);

  -- Clock generated
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  -- Bit stream generated (sequence: 1011)
  d_in_process : process
  begin
    wait for clk_period/2;
    -- Inserts 1 to MSB
    d_in <= std_logic_vector(to_unsigned(1, k));
    wait for clk_period;
    -- Inserts 0 to MSB
    d_in <= std_logic_vector(to_unsigned(0, k));
    wait for clk_period;
    -- Inserts 1 to MSB
    d_in <= std_logic_vector(to_unsigned(1, k));
    wait for clk_period;
    -- Inserts 1 to MSB
    d_in <= std_logic_vector(to_unsigned(1, k));
    wait for clk_period;
    -- Data is zero (trail shifting)
    d_in <= std_logic_vector(to_unsigned(0, k));
    wait for clk_period;
    -- Data is zero (trail shifting)
    d_in <= std_logic_vector(to_unsigned(0, k));
    wait for clk_period;
    -- Data is zero (trail shifting)
    d_in <= std_logic_vector(to_unsigned(0, k));
    wait for 3*clk_period/2;
  end process;

end Behavioral;
