-- Company: Fachhochschule Dortmund
-- Engineer: Javier Reyes
--
-- Create Date: 06/16/2017 04:49:58 PM
-- Design Name: Shift register for Convolutional Codes example project
-- Module Name: shift_reg - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity shift_reg is
  port (sr_clk  : in std_logic;
        sr_rst  : in std_logic;
        sr_in   : in std_logic_vector(k-1 downto 0);
        sr_data : out std_logic_vector(m-1 downto 0));
        -- MSB is data, LSB is data at t-3
end shift_reg;

architecture Behavioral of shift_reg is
-- Signals declaration
signal data : std_logic_vector(m-1 downto 0) := (others => '0');

begin
  -- Shifting the bits inside signal 'data'
  sr_data <= data;

  -- Shifting process with asynchronous reset
  process (sr_clk, sr_rst)
  begin
    if (sr_rst = '1') then
      -- If 'reset' is true, output is zero
      data <= (others => '0');
    elsif (rising_edge(sr_clk)) then
      data(0) <= data(1);
      data(1) <= data(2);
      data(2) <= data(3);
      data(3) <= sr_in(0);
    end if;
  end process;
end Behavioral;
