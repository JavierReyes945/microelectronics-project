-- Company: Fachhochschule Dortmund
-- Engineer: Javier Reyes
--
-- Create Date: 06/16/2017 02:54:48 PM
-- Design Name: Encoder testbench for Convolutional Codes example project
-- Module Name: encoder_tb - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity encoder_tb is
  -- EMPTY
end encoder_tb;

architecture Behavioral of encoder_tb is

-- Signals declaration
signal in_data  : std_logic_vector(k-1 downto 0) := std_logic_vector(to_unsigned(0, k));
signal clk      : std_logic := '0';
signal rs       : std_logic := '0';
signal out_data : std_logic_vector(n-1 downto 0);

-- Components declaration
component encoder
  port (enc_in  : in std_logic_vector(k-1 downto 0);
        enc_clk : in std_logic;
        enc_rs  : in std_logic;
        enc_out : out std_logic_vector(n-1 downto 0));
end component encoder;

begin

  -- instantiating the components
  ENC : encoder port map (in_data, clk, rs, out_data);

  -- Clock generated
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  -- Bit stream generated (sequence: 1011)
  d_in_process : process
  begin
    wait for clk_period/2;
    -- Inserts 1 to MSB
    in_data <= std_logic_vector(to_unsigned(1, k));
    wait for clk_period;
    -- Inserts 0 to MSB
    in_data <= std_logic_vector(to_unsigned(0, k));
    wait for clk_period;
    -- Inserts 1 to MSB
    in_data <= std_logic_vector(to_unsigned(1, k));
    wait for clk_period;
    -- Inserts 1 to MSB
    in_data <= std_logic_vector(to_unsigned(1, k));
    wait for clk_period;
    -- Data is zero (trail shifting)
    in_data <= std_logic_vector(to_unsigned(0, k));
    wait for clk_period;
    -- Data is zero (trail shifting)
    in_data <= std_logic_vector(to_unsigned(0, k));
    wait for clk_period;
    -- Data is zero (trail shifting)
    in_data <= std_logic_vector(to_unsigned(0, k));
    wait for 3*clk_period/2;
  end process;

end Behavioral;
