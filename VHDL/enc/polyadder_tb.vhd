-- Company: Fachhochschule Dortmund
-- Engineer: Javier Reyes
--
-- Create Date: 06/16/2017 04:49:58 PM
-- Design Name: Polynomial Adder Testbench for Convolutional Codes example project
-- Module Name: polyadder_tb - Simulation
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity polyadder_tb is
  -- EMPTY
end polyadder_tb;

architecture Behavioral of polyadder_tb is
-- Signals declaration
-- MSB is input of shift register
signal d_in   : std_logic_vector(m-1 downto 0);
signal d_out  : std_logic_vector(n-1 downto 0);

-- Components declaration
component polyadder
  port (add_in  : in std_logic_vector(m-1 downto 0);
        add_out : out std_logic_vector(n-1 downto 0));
end component;

begin
  -- instantiating the components
  POLADD : polyadder port map (d_in, d_out);

  -- Bit stream generated (emulate output of shift register)
  d_in_process : process
  begin
    -- Initialized as zero
    d_in <= std_logic_vector(to_unsigned(0, m));
    wait for 2*clk_period;
    -- Insert 1, value 1000 = 8
    d_in <= std_logic_vector(to_unsigned(8, m));
    wait for clk_period;
    -- Insert 0, value 0100 = 4
    d_in <= std_logic_vector(to_unsigned(4, m));
    wait for clk_period;
    -- Insert 1, value 1010 = 12
    d_in <= std_logic_vector(to_unsigned(10, m));
    wait for clk_period;
    -- Insert 1, value 1101 = 13
    d_in <= std_logic_vector(to_unsigned(13, m));
    wait for clk_period;
    -- Insert 0, value 0110 = 6 (trail)
    d_in <= std_logic_vector(to_unsigned(6, m));
    wait for clk_period;
    -- Insert 0, value 0011 = 3 (trail)
    d_in <= std_logic_vector(to_unsigned(3, m));
    wait for clk_period;
    -- Insert 0, value 0001 = 1 (trail)
    d_in <= std_logic_vector(to_unsigned(1, m));
    wait for clk_period;
    -- Insert 0, value 0000 = 0 (trail)
    d_in <= std_logic_vector(to_unsigned(0, m));
    wait for clk_period;
  end process;
end Behavioral;
