-- Company: Fachhochschule Dortmund
-- Engineer: Javier Reyes
--
-- Create Date: 26/06/2017 23:46:52 PM
-- Design Name: Polynomial Adder for Convolutional Codes example project
-- Module Name: polyadder - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity polyadder is
  port (add_in  : in std_logic_vector(m-1 downto 0);
        add_out : out std_logic_vector(n-1 downto 0));
end polyadder;

architecture Behavioral of polyadder is

begin
  process (add_in)
  -- Temporal variables
  variable temp : std_logic_vector(m-1 downto 0);
  variable aux  : std_logic;

  begin
    out_counter : for i in 0 to n-1 loop
      -- Masks the bits (Polynomial)
      temp := add_in and pol(i);
      -- Copies the LSB bit of the vector
      aux := temp(0);
      bit_counter : for j in 1 to m-1 loop
        -- Sequencially adds the bits inside temp
        aux := temp(j) xor aux;
      end loop;
      -- Copies the result to output
      add_out(i) <= aux;
    end loop;
  end process;
end Behavioral;
