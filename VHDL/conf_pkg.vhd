-- Company: Fachhochschule Dortmund
-- Engineer: Javier Reyes
--
-- Create Date: 27/06/2017 10:20:32 AM
-- Design Name: Configuration package for Convolutional Codes example project
-- Module Name: conf_pkg - Package
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package conf_pkg is
	-- Defined number of output bits
	constant n : integer := 2;
	-- Defined number of input bits
	constant k : integer := 1;
	-- Defined number of bits for the shift register
	constant m : integer := 4;

	-- Defined number of bits for the input sequence
	constant seq : integer := (2*m)-1;

	-- Type plynomial declaration
	type poly is array(n-1 downto 0) of std_logic_vector(m-1 downto 0);

	-- Constant declaration generator polynomial
	-- MSB is input of shift register
	constant pol : poly := ("1111", "1101");

	-- Defined simulation clock period
	constant clk_period : time := 1 ns;
end package;
