-- Company: Fachhochschule Dortmund
-- Engineer: Mysara Ibrahim
--
-- Create Date: 27/06/2017 10:20:32 AM
-- Design Name: Noise module for Convolutional Codes example project
-- Module Name: noise - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity noise is
  port (nin  : in  std_logic_vector(n-1 downto 0);
        nout : out std_logic_vector(n-1 downto 0));
end noise ;

architecture Behavioral of noise is

begin
  nout <= nin;
  --process (nin)
  --variable tempo : std_logic_vector(n-1 downto 0) := (others => '0');
  --begin

    --if (nin(0) /= nin(1)) and (tempo = "00") then
    --  nout(0) <= not nin(0);
    --  nout(1) <=  nin(1);
    --  tempo:= "11";
    --else
    --  nout <= nin;
    --end if;
  --end process;

end Behavioral;
