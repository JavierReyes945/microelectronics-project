-- Company: Fachhochschule Dortmund
-- Engineer: Mysara Ibrahim
--
-- Create Date: 27/06/2017 10:20:32 AM
-- Design Name: Traceback for Convolutional Codes example project
-- Module Name: MyTraceBack - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity MyTraceBack is
  port (index : in integer;
        surv_path : in integer;
        selectionIndex1, selectionIndex2, selectionIndex3, selectionIndex4 : in bit;
        selectionIndex5, selectionIndex6, selectionIndex7, selectionIndex8 : in bit;
        Decoded_out : out std_logic_vector(6 downto 0) := (others => '0'));
end MyTraceBack;

architecture Behavioral of MyTraceBack is

type selectm is array(7 downto 0, 3 downto 0) of bit;
shared variable sel : selectm := ((others=>(others=> '0' )));

begin

  process (index)
  begin
      if (index>5) then
        sel(0,index-6) := selectionIndex1;
        sel(1,index-6) := selectionIndex2;
        sel(2,index-6) := selectionIndex3;
        sel(3,index-6) := selectionIndex4;
        sel(4,index-6) := selectionIndex5;
        sel(5,index-6) := selectionIndex6;
        sel(6,index-6) := selectionIndex7;
        sel(7,index-6) := selectionIndex8;
      end if;
  end process;

  process (surv_path)

  variable temp : integer :=0;
  variable result : std_logic :='0';
  variable jlooper : integer := 0;
  variable ilooper : integer := 4;
  
  begin
          if (surv_path>0)then

  if (index > 0) then
      ilooper:= 3;
      jlooper := 0;
      temp:=surv_path;
      l1 : while ilooper >= 0 loop
        l2 : while jlooper < 8 loop
          if (jlooper=temp-1) then
            if (sel(jlooper,ilooper)='1') then
              case temp is
                when 1 => result :='0'; temp:=2; jlooper:=10;
                when 2 => result :='0'; temp:=4; jlooper:=10;
                when 3 => result :='0'; temp:=6; jlooper:=10;
                when 4 => result :='0'; temp:=8; jlooper:=10;
                when 5 => result :='1'; temp:=2; jlooper:=10;
                when 6 => result :='1'; temp:=4; jlooper:=10;
                when 7 => result :='1'; temp:=6; jlooper:=10;
                when 8 => result :='1'; temp:=8; jlooper:=10;
                when others => result := '0'; jlooper:=10;
              end case;
--            end if;
--            if (sel(jlooper,ilooper)='0') then
              else
              case temp is
                when 1 => result :='0'; temp:=1; jlooper:=10;
                when 2 => result :='0'; temp:=3; jlooper:=10;
                when 3 => result :='0'; temp:=5; jlooper:=10;
                when 4 => result :='0'; temp:=7; jlooper:=10;
                when 5 => result :='1'; temp:=1; jlooper:=10;
                when 6 => result :='1'; temp:=3; jlooper:=10;
                when 7 => result :='1'; temp:=5; jlooper:=10;
                when 8 => result :='1'; temp:=7; jlooper:=10;
                when others => result := '0'; jlooper:=10;
              end case;
            end if;
          end if;
          jlooper:=jlooper+1;
        end loop l2;
        jlooper := 0;
        Decoded_out(3+ilooper) <= result;
        ilooper:=ilooper-1;
      end loop l1;
      
      case temp is
        when 1 => Decoded_out(2) <= '0';Decoded_out(1) <= '0';Decoded_out(0) <= '0';
        when 2 => Decoded_out(2) <= '0';Decoded_out(1) <= '0';Decoded_out(0) <= '1';
        when 3 => Decoded_out(2) <= '0';Decoded_out(1) <= '1';Decoded_out(0) <= '0';
        when 4 => Decoded_out(2) <= '0';Decoded_out(1) <= '1';Decoded_out(0) <= '1';
        when 5 => Decoded_out(2) <= '1';Decoded_out(1) <= '0';Decoded_out(0) <= '0';
        when 6 => Decoded_out(2) <= '1';Decoded_out(1) <= '0';Decoded_out(0) <= '1';
        when 7 => Decoded_out(2) <= '1';Decoded_out(1) <= '1';Decoded_out(0) <= '0';
        when 8 => Decoded_out(2) <= '1';Decoded_out(1) <= '1';Decoded_out(0) <= '1';
        when others => Decoded_out(2) <= '0';Decoded_out(1) <= '0';Decoded_out(0) <= '0';
      end case;      
   end if;
   end if;
  end process;

end Behavioral;
