-- Company: Fachhochschule Dortmund
-- Engineer: Mysara Ibrahim
--
-- Create Date: 27/06/2017 10:20:32 AM
-- Design Name: Comparator Unit for Convolutional Codes example project
-- Module Name: MyComparator - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity MyComparator_tb is
  -- EMPTY
end;

architecture Behavioral of MyComparator_tb is

-- Signals declaration
signal IN1: integer;
signal IN2: integer;
signal Bigger: integer;
signal Comp_clk: std_logic;
signal selectionIndex: bit;

-- Components declaration
component MyComparator
  port (IN1: in  integer;
        IN2 : in  integer;
        Bigger : out integer;
        Comp_clk: in  std_logic;
        selectionIndex : out bit);
end component;

begin

  uut: MyComparator port map ( IN1            => IN1,
                               IN2            => IN2,
                               Bigger         => Bigger,
                               Comp_clk       => Comp_clk,
                               selectionIndex => selectionIndex);

  -- Clock generated
  clk_process : process
  begin
    Comp_clk <= '0';
    wait for clk_period/2;
    Comp_clk <= '1';
    wait for clk_period/2;
  end process;

-- Stimulus generated
  stim_proc : process
  begin
    wait for clk_period/2;
    IN1 <= 1;
    IN2 <= 2;
    wait for clk_period;
    IN1 <= 2;
    IN2 <= 1;
    wait for clk_period;
    IN1 <= 2;
    IN2 <= 2;
    wait for clk_period;
  end process;

end Behavioral;
