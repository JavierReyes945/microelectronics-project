-- Company: Fachhochschule Dortmund
-- Engineer: Mysara Ibrahim
--
-- Create Date: 27/06/2017 10:20:32 AM
-- Design Name: Decoder for Convolutional Codes example project
-- Module Name: decoder - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity decoder is
  port (EncIN   : in  std_logic_vector(n-1 downto 0);
        Dec_clk : in  std_logic;
        DecOUT  : out std_logic_vector(6 downto 0));
end decoder;

architecture Behavioral of decoder is

-- Signals declaration
signal clk : integer;
signal BmuAdder1, BmuAdder2, BmuAdder3, BmuAdder4 : integer;
signal BmuAdder5, BmuAdder6, BmuAdder7, BmuAdder8 : integer;
signal BmuAdder9, BmuAdder10, BmuAdder11, BmuAdder12 : integer;
signal BmuAdder13, BmuAdder14, BmuAdder15, BmuAdder16 : integer;
signal selectx1, selectx2, selectx3, selectx4 :  bit;
signal selectx5, selectx6, selectx7, selectx8 :  bit;
signal OUP1_1, OUP1_5, OUP5_3, OUP5_7, OUP3_2, OUP3_6, OUP7_4, OUP7_8 :  integer;
signal OUP2_1, OUP2_5, OUP6_3, OUP6_7, OUP4_2, OUP4_6, OUP8_4, OUP8_8 :  integer;
signal PMX1, PMX2, PMX3, PMX4, PMX5, PMX6, PMX7, PMX8 :  integer;
signal path1, path2, path3, path4, path5, path6, path7, path8 : integer;
signal surv_path, index1 : integer;

-- Components declaration
component MyBMU
  port (INP     : in  std_logic_vector (n-1 downto 0);
        BMU_clk : in  std_logic;
        Index   : out integer;
        OUP1, OUP2, OUP3, OUP4, OUP5, OUP6, OUP7, OUP8, OUP9, OUP10,
        OUP11, OUP12, OUP13, OUP14, OUP15, OUP16 : out integer := 0);
end component;

component MyADDER
  port (INP1, INP2, INP3, INP4, INP5, INP6, INP7, INP8, INP9, INP10,
        INP11, INP12, INP13, INP14, INP15, INP16 : in integer;
        ADDer_clk : in std_logic;
        ADDer_count : in integer;
        OUP1_1, OUP1_5, OUP5_3, OUP5_7, OUP3_2, OUP3_6, OUP7_4, OUP7_8, OUP2_1,
        OUP2_5, OUP6_3, OUP6_7, OUP4_2, OUP4_6, OUP8_4, OUP8_8 : out integer :=0);
end component;

component MyComparator
  port (IN1 : in integer;
       IN2 : in integer;
       Bigger : out integer := 0;
       Comp_clk : in std_logic;
       Comp_count : in integer;           
       selectionIndex : out bit := '0');
end component;

component surviv_path
  port (path1, path2, path3, path4, path5, path6, path7, path8 : in integer;
        index     : in  integer;
        surv_path : out integer := 0);
end component;

component MyTraceBack
  port (index : in integer;
        surv_path : in integer;
        selectionIndex1, selectionIndex2, selectionIndex3, selectionIndex4 : in bit;
        selectionIndex5, selectionIndex6, selectionIndex7, selectionIndex8 : in bit;
        Decoded_out : out std_logic_vector(6 downto 0) := (others => '0'));
end component;

begin

  MyBMU1 : MyBMU port map(EncIN, Dec_clk, index1,
                          BmuAdder1, BmuAdder2, BmuAdder3, BmuAdder4,
                          BmuAdder5, BmuAdder6, BmuAdder7, BmuAdder8,
                          BmuAdder9, BmuAdder10, BmuAdder11, BmuAdder12,
                          BmuAdder13, BmuAdder14, BmuAdder15, BmuAdder16);

  MyADDER1 : MyADDER port map(BmuAdder1, BmuAdder2, BmuAdder3, BmuAdder4,
                              BmuAdder5, BmuAdder6, BmuAdder7, BmuAdder8,
                              BmuAdder9, BmuAdder10, BmuAdder11, BmuAdder12,
                              BmuAdder13, BmuAdder14, BmuAdder15, BmuAdder16,
                              Dec_clk, index1,
                              OUP1_1, OUP1_5, OUP5_3, OUP5_7, OUP3_2, OUP3_6, OUP7_4, OUP7_8,
                              OUP2_1, OUP2_5, OUP6_3, OUP6_7, OUP4_2, OUP4_6, OUP8_4, OUP8_8);

  comp1 : MyComparator port map(OUP1_1, OUP2_1, PMX1, Dec_clk, index1, selectx1);
  comp2 : MyComparator port map(OUP3_2, OUP4_2, PMX2, Dec_clk, index1, selectx2);
  comp3 : MyComparator port map(OUP5_3, OUP6_3, PMX3, Dec_clk, index1, selectx3);
  comp4 : MyComparator port map(OUP7_4, OUP8_4, PMX4, Dec_clk, index1, selectx4);
  comp5 : MyComparator port map(OUP1_5, OUP2_5, PMX5, Dec_clk, index1, selectx5);
  comp6 : MyComparator port map(OUP3_6, OUP4_6, PMX6, Dec_clk, index1, selectx6);
  comp7 : MyComparator port map(OUP5_7, OUP6_7, PMX7, Dec_clk, index1, selectx7);
  comp8 : MyComparator port map(OUP7_8, OUP8_8, PMX8, Dec_clk, index1, selectx8);

  surv : surviv_path port map(PMX1, PMX2, PMX3, PMX4, PMX5, PMX6, PMX7, PMX8, index1, surv_path);

  resultbts : MyTraceBack port map( index1, surv_path,
                                    selectx1, selectx2, selectx3, selectx4,
                                    selectx5, selectx6, selectx7, selectx8,
                                    DecOUT);

end Behavioral;
