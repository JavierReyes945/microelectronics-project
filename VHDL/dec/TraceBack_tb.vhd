-- Company: Fachhochschule Dortmund
-- Engineer: Mysara Ibrahim
--
-- Create Date: 06/16/2017 03:07:27 PM
-- Design Name: TraceBack testbench for Convolutional Codes example project
-- Module Name: TraceBack_tb - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity TraceBack_tb is
--  Port ( );
end TraceBack_tb;

architecture Behavioral of TraceBack_tb is

component MyTraceBack
  port (index : in integer;
        surv_path : in integer;
        selectionIndex1, selectionIndex2, selectionIndex3, selectionIndex4 : in bit;
        selectionIndex5, selectionIndex6, selectionIndex7, selectionIndex8 : in bit;
        Decoded_out : out std_logic_vector(seq downto 0):=(others => '0'));
end component MyTraceBack;

signal selectx1, selectx2, selectx3, selectx4, selectx5, selectx6, selectx7, selectx8 :  bit;
signal surv_sig, index1 : integer range 0 to 8;
signal trac_out : std_logic_vector(seq downto 0):="00000000";

begin

  resbts : MyTraceBack port map(index => index1, surv_path => surv_sig,
                                selectionIndex1 => selectx1, selectionIndex2 => selectx2,
                                selectionIndex3 => selectx3, selectionIndex4 => selectx4,
                                selectionIndex5 => selectx5, selectionIndex6 => selectx6,
                                selectionIndex7 => selectx7, selectionIndex8 => selectx8,
                                Decoded_out => trac_out);

  stim_proc : process
  begin
    wait for clk_period/2;
    index1 <= 0;
        selectx1 <= '1';selectx2 <= '1';selectx3 <= '1';selectx4 <= '1';selectx5 <= '1';selectx6 <= '1';selectx7 <= '1';selectx8 <= '1';
    wait for clk_period;
    index1 <= index1+1;
        selectx1 <= '1';selectx2 <= '1';selectx3 <= '1';selectx4 <= '1';selectx5 <= '0';selectx6 <= '1';selectx7 <= '1';selectx8 <= '1';
    wait for clk_period;
    index1 <= index1+1;
        selectx1 <= '1';selectx2 <= '1';selectx3 <= '0';selectx4 <= '1';selectx5 <= '1';selectx6 <= '1';selectx7 <= '1';selectx8 <= '1';
    wait for clk_period;
    index1 <= index1+1;
        selectx1 <= '1';selectx2 <= '1';selectx3 <= '1';selectx4 <= '1';selectx5 <= '1';selectx6 <= '0';selectx7 <= '1';selectx8 <= '1';
    wait for clk_period;
    index1 <= index1+1;
        selectx1 <= '1';selectx2 <= '1';selectx3 <= '1';selectx4 <= '1';selectx5 <= '1';selectx6 <= '1';selectx7 <= '1';selectx8 <= '1';
    wait for clk_period;
    index1 <= index1+1;
        selectx1 <= '1';selectx2 <= '1';selectx3 <= '1';selectx4 <= '0';selectx5 <= '1';selectx6 <= '1';selectx7 <= '1';selectx8 <= '1';
    wait for clk_period;
    index1 <= index1+1;
        selectx1 <= '1';selectx2 <= '1';selectx3 <= '1';selectx4 <= '1';selectx5 <= '1';selectx6 <= '1';selectx7 <= '1';selectx8 <= '1';
    wait for clk_period;
    index1 <= index1+1; surv_sig <= 1;
        selectx1 <= '1';selectx2 <= '1';selectx3 <= '1';selectx4 <= '1';selectx5 <= '1';selectx6 <= '1';selectx7 <= '1';selectx8 <= '1';
    wait for 3*clk_period/2;
  end process;

end Behavioral;
