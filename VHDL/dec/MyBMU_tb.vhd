-- Company: Fachhochschule Dortmund
-- Engineer: Javier Reyes
--
-- Create Date: 06/16/2017 02:54:48 PM
-- Design Name: Branc Metric Unit testbench for Convolutional Codes example project
-- Module Name: MyBMU_tb - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity MyBMU_tb is
  -- EMPTY
end MyBMU_tb;

architecture Behavioral of MyBMU_tb is

-- Signals declaration
signal in_data : std_logic_vector(n-1 downto 0) := (others => '0');
signal clk     : std_logic := '0';
signal indx    : integer;
signal OUP1, OUP2, OUP3, OUP4, OUP5, OUP6, OUP7, OUP8 : integer;
signal OUP9, OUP10, OUP11, OUP12, OUP13, OUP14, OUP15, OUP16 : integer;

-- Components declaration
component MyBMU
  port (INP     : in  std_logic_vector (n-1 downto 0);
        BMU_clk : in  std_logic;
        Index   : out integer;
        OUP1, OUP2, OUP3, OUP4, OUP5, OUP6, OUP7, OUP8 : out integer;
        OUP9, OUP10, OUP11, OUP12, OUP13, OUP14, OUP15, OUP16 : out integer );
end component;

begin

  -- instantiating the components
  bmu1 : MyBMU port map(in_data, clk, indx,
                        OUP1, OUP2, OUP3, OUP4, OUP5, OUP6, OUP7, OUP8,
                        OUP9, OUP10, OUP11, OUP12, OUP13, OUP14, OUP15, OUP16);

  -- Clock generated
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  -- Stimulus generated
  stim_proc : process
  begin
    wait for clk_period/2;
    in_data <= "11";
    wait for clk_period;
    in_data <= "11";
    wait for clk_period;
    in_data <= "01";
    wait for clk_period;
    in_data <= "11";
    wait for clk_period;
    in_data <= "01";
    wait for clk_period;
    in_data <= "01";
    wait for clk_period;
    in_data <= "11";
    wait for 3*clk_period/2;
  end process;

end Behavioral;
