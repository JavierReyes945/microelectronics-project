-- Company: Fachhochschule Dortmund
-- Engineer: Mysara Ibrahim
--
-- Create Date: 27/06/2017 10:20:32 AM
-- Design Name: Branch metric Unit for Convolutional Codes example project
-- Module Name: MyBMU - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity MyBMU is
  port (INP     : in  std_logic_vector (n-1 downto 0);
        BMU_clk : in  std_logic;
        Index   : out integer;
        OUP1, OUP2, OUP3, OUP4, OUP5, OUP6, OUP7, OUP8 : out integer := 0;
        OUP9, OUP10, OUP11, OUP12, OUP13, OUP14, OUP15, OUP16 : out integer := 0);
end MyBMU;

architecture Behavioral of MyBMU is

-- Function declaration
function hamDist(h_in : std_logic_vector(n-1 downto 0)) return integer is
begin
  case h_in is
    when "00" =>
      return 2;
    when "01" =>
      return 1;
    when "10" =>
      return 1;
    when "11" =>
      return 0;
    when others =>
      return -1; --invalid operation
 end case;
end function;

begin
  -- Process to update Index value and Hamming distance
  update : process (BMU_clk)
  -- Variables declaration
  variable count : integer:=0;
  
  begin
    if (rising_edge(BMU_clk)) then
    
        Index<=count;
	    count :=count +1;
      if (count = 10) then
        count := 0;
      end if;
      -- Hamming distance calculation for all branches
        OUP1  <= hamDist(INP XOR "00"); OUP2  <= hamDist(INP XOR "11");
    	OUP3  <= hamDist(INP XOR "11"); OUP4  <= hamDist(INP XOR "00");
    	OUP5  <= hamDist(INP XOR "10"); OUP6  <= hamDist(INP XOR "01");
    	OUP7  <= hamDist(INP XOR "01"); OUP8  <= hamDist(INP XOR "10");
    	OUP9  <= hamDist(INP XOR "11"); OUP10 <= hamDist(INP XOR "00");
    	OUP11 <= hamDist(INP XOR "00"); OUP12 <= hamDist(INP XOR "11");
    	OUP13 <= hamDist(INP XOR "01"); OUP14 <= hamDist(INP XOR "10");
    	OUP15 <= hamDist(INP XOR "10"); OUP16 <= hamDist(INP XOR "01");
    end if;
  end process;
end Behavioral;
