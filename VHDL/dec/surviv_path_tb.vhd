-- Company: Fachhochschule Dortmund
-- Engineer: Mysara Ibrahim
--
-- Create Date: 27/06/2017 10:20:32 AM
-- Design Name: Survivor Path for Convolutional Codes example project
-- Module Name: surviv_path - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;
use work.conf_pkg.all;

entity surviv_path_tb is
  -- EMPTY
end;

architecture bench of surviv_path_tb is

-- Signals declaration
signal path1, path2, path3, path4, path5, path6, path7, path8: integer range 0 to 16;
signal index: integer range 0 to 8;
signal surv_path: integer := 0;

-- Components declaration
component surviv_path
  port (path1, path2, path3, path4, path5, path6, path7, path8 : in integer range 0 to 16;
        index     : in  integer range 0 to 8;
        surv_path : out integer := 0);
end component;

begin

  uut: surviv_path port map ( path1     => path1,
                              path2     => path2,
                              path3     => path3,
                              path4     => path4,
                              path5     => path5,
                              path6     => path6,
                              path7     => path7,
                              path8     => path8,
                              index     => index,
                              surv_path => surv_path );

  stimulus: process
  begin
    --1
    index<=7;
    path1<=14;
    path2<=3;
    path3<=11;
    path4<=3;
    path5<=12;
    path6<=3;
    path7<=10;
    path8<=2;
    wait for clk_period*2;

    index<=1;
    wait for clk_period*2;

    --2
    index<=7;
    path1<=10;
    path2<=13;
    path3<=11;
    path4<=3;
    path5<=12;
    path6<=3;
    path7<=10;
    path8<=2;
    wait for clk_period*2;

    index<=1;
    wait for clk_period*2;

    --3
    index<=7;
    path1<=1;
    path2<=3;
    path3<=11;
    path4<=3;
    path5<=10;
    path6<=3;
    path7<=10;
    path8<=2;
    wait for clk_period*2;

    index<=1;
    wait for clk_period*2;

    --4
    index<=7;
    path1<=1;
    path2<=1;
    path3<=11;
    path4<=13;
    path5<=12;
    path6<=3;
    path7<=10;
    path8<=2;
    wait for clk_period*2;

    index<=1;
    wait for clk_period*2;

    --5
    index<=7;
    path1<=12;
    path2<=3;
    path3<=12;
    path4<=3;
    path5<=13;
    path6<=3;
    path7<=10;
    path8<=2;
    wait for clk_period*2;

    index<=1;
    wait for clk_period*2;

    --6
    index<=7;
    path1<=11;
    path2<=11;
    path3<=11;
    path4<=11;
    path5<=11;
    path6<=12;
    path7<=11;
    path8<=11;
    wait for clk_period*2;

    index<=1;
    wait for clk_period*2;

    --7
    index<=7;
    path1<=1;
    path2<=3;
    path3<=11;
    path4<=3;
    path5<=12;
    path6<=3;
    path7<=13;
    path8<=2;
    wait for clk_period*2;

    index<=1;
    wait for clk_period*2;

    --8
    index<=7;
    path1<=11;
    path2<=3;
    path3<=11;
    path4<=3;
    path5<=11;
    path6<=3;
    path7<=10;
    path8<=12;
    wait for clk_period*2;

    index<=1;
    wait for clk_period*2;

    wait;
  end process;

end;
