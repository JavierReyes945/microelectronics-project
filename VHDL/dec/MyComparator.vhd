-- Company: Fachhochschule Dortmund
-- Engineer: Mysara Ibrahim
--
-- Create Date: 27/06/2017 10:20:32 AM
-- Design Name: Comparator Unit for Convolutional Codes example project
-- Module Name: MyComparator - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity MyComparator is
    Port ( IN1 : in integer;
           IN2 : in integer;
           Bigger : out integer := 0;
           Comp_clk : in std_logic;
           Comp_count : in integer;
           selectionIndex : out bit := '0');
end MyComparator;

architecture Behavioral of MyComparator is

begin

  process (Comp_clk)
  begin
    if (falling_edge(Comp_clk)) then
      if (Comp_count>=0) then
        if (Comp_count > 3) then
          if (IN1>IN2) then
            Bigger<=IN1;
            selectionIndex<='0';
          else
            Bigger<=IN2;
            selectionIndex<='1';
          end if;
        end if;
      end if;
    end if;
  end process;

end Behavioral;
