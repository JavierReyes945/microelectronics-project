-- Company: Fachhochschule Dortmund
-- Engineer: Mysara Ibrahim
--
-- Create Date: 27/06/2017 10:20:32 AM
-- Design Name: Survivor Path for Convolutional Codes example project
-- Module Name: surviv_path - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity surviv_path is
  port (path1, path2, path3, path4, path5, path6, path7, path8 : in integer;
        index     : in  integer;
        surv_path : out integer);
end surviv_path;

architecture Behavioral of surviv_path is

begin

  process (index)
  begin
    if (index = 9) then
      if ((path1 >= path2) and (path1 >= path3) and (path1 >= path4) and (path1 >= path5) and
          (path1 >= path6) and (path1 >= path7) and(path1 >= path8)) then
        surv_path <= 1;
      end if;
      if ((path2 >= path1) and (path2 >= path3) and (path2 >= path4) and (path2 >= path5) and
          (path2 >= path6) and (path2 >= path7) and (path2 >= path8)) then
        surv_path <= 2;
      end if;
      if ((path3 >= path1) and (path3 >= path2) and (path3 >= path4) and (path3 >= path5) and
          (path3 >= path6) and (path3 >= path7) and (path3 >= path8) ) then
        surv_path <= 3;
      end if;
      if ((path4 >= path1) and (path4 >= path2) and (path4 >= path3) and (path4 >= path5) and
          (path4 >= path6) and (path4 >= path7) and (path4 >= path8)) then
        surv_path <= 4;
      end if;
      if ((path5 >= path1) and (path5 >= path2) and (path5 >= path3) and (path5 >= path4) and
          (path5 >= path6) and (path5 >= path7) and (path5 >= path8)) then
        surv_path <= 5;
      end if;
      if ((path6 >= path1) and (path6 >= path2) and (path6 >= path3) and (path6 >= path4) and
          (path6 >= path5) and (path6 >= path7) and (path6 >= path8)) then
        surv_path <= 6;
      end if;
      if ((path7 >= path1) and (path7 >= path2) and (path7 >= path3) and (path7 >= path4) and
          (path7 >= path5) and (path7 >= path6) and (path7 >= path8)) then
        surv_path <= 7;
      end if;
      if ((path8 >= path1) and (path8 >= path2) and (path8 >= path3) and (path8 >= path4) and
          (path8 >= path5) and (path8 >= path6) and (path8 >= path1)) then
        surv_path <= 8;
      end if;
    end if;
  end process;

end Behavioral;
