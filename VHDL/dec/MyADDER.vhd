-- Company: Fachhochschule Dortmund
-- Engineer: Mysara Ibrahim
--
-- Create Date: 27/06/2017 10:20:32 AM
-- Design Name: Adder for Convolutional Codes example project
-- Module Name: MyADDER - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity MyADDER is
  port (INP1, INP2, INP3, INP4, INP5, INP6, INP7, INP8 : in integer;
        INP9, INP10, INP11, INP12, INP13, INP14, INP15, INP16 : in integer;
        ADDer_clk : in std_logic;
        ADDer_count : in integer;
        OUP1_1, OUP1_5, OUP5_3, OUP5_7, OUP3_2, OUP3_6, OUP7_4, OUP7_8 : out integer:=0;
        OUP2_1, OUP2_5, OUP6_3, OUP6_7, OUP4_2, OUP4_6, OUP8_4, OUP8_8 : out integer :=0);
end MyADDER;

architecture Behavioral of MyADDER is

begin

  process (ADDer_clk)

  type matrix is array(seq downto 0) of integer;
  variable sum : matrix := ((0),(0),(0),(0),(0),(0),(0),(0));
  variable sum_old : matrix := ((0),(0),(0),(0),(0),(0),(0),(0));

  begin
    if (falling_edge(ADDer_clk)) then

      if (ADDer_count> 0) then
        OUP1_1 <= INP1 + sum_old(0);
        if (ADDer_count <= 3) then  sum(0) := sum_old(0) + INP1; end if;
        OUP1_5 <= INP2 + sum_old(0);
        if (ADDer_count <= 3) then  sum(4) := sum_old(0) + INP2; end if;
        if (ADDer_count > 1) then
          OUP5_3 <= INP9 + sum_old(4);
          if (ADDer_count <= 3) then  sum(2) := sum_old(4) + INP9; end if;
          OUP5_7 <= INP10 + sum_old(4);
          if (ADDer_count <= 3) then  sum(6) := sum_old(4) + INP10; end if;
          if (ADDer_count > 2) then
            OUP3_2 <= INP5 + sum_old(2);
            if (ADDer_count <= 3) then  sum(1) := sum_old(2) + INP5; end if;
            OUP3_6 <= INP6 + sum_old(2);
            if (ADDer_count <= 3) then  sum(5) := sum_old(2) + INP6; end if;
            OUP7_4 <= INP13 + sum_old(6);
            if (ADDer_count <= 3) then  sum(3) := sum_old(6) + INP13; end if;
            OUP7_8 <= INP14 + sum_old(6);
            if (ADDer_count <= 3) then  sum(7) := sum_old(6) + INP14; end if;
            if (ADDer_count > 3) then
              OUP2_1 <= INP3 + sum_old(1);
              OUP2_5 <= INP4 + sum_old(1);
              OUP6_3 <= INP11 + sum_old(5);
              OUP6_7 <= INP12 + sum_old(5);
              OUP4_2 <= INP7 + sum_old(3);
              OUP4_6 <= INP8 + sum_old(3);
              OUP8_4 <= INP15 + sum_old(7);
              OUP8_8 <= INP16 + sum_old(7);
            end if;
          end if;
        end if;
        if (ADDer_count > 3) then

          if ((sum_old(0) + INP1)>(sum_old(1) + INP3)) then
            sum(0) := sum_old(0) + INP1;
          else sum(0) := sum_old(1) + INP3; end if;
          if ((sum_old(0) + INP2)>(sum_old(1) + INP4)) then
            sum(4) := sum_old(0) + INP2;
          else sum(4) := sum_old(1) + INP4; end if;
          if ((sum_old(4) + INP9)>(sum_old(5) + INP11)) then
            sum(2) := sum_old(4) + INP9;
          else sum(2) := sum_old(5) + INP11; end if;
          if ((sum_old(4) + INP10)>(sum_old(5) + INP12)) then
            sum(6) := sum_old(4) + INP10;
          else sum(6) := sum_old(5) + INP12; end if;
          if ((sum_old(2) + INP5)>(sum_old(3) + INP7)) then
            sum(1) := sum_old(2) + INP5;
          else sum(1) := sum_old(3) + INP7; end if;
          if ((sum_old(2) + INP6)>(sum_old(3) + INP8)) then
            sum(5) := sum_old(2) + INP6;
          else sum(5) := sum_old(3) + INP8; end if;
          if ((sum_old(6) + INP13)>(sum_old(7) + INP15)) then
            sum(3) := sum_old(6) + INP13
           else sum(3) := sum_old(7) + INP15; end if;
          if ((sum_old(6) + INP14)>(sum_old(7) + INP16)) then
            sum(7) := sum_old(6) + INP14
          else sum(7) := sum_old(7) + INP16; end if;

        end if;
        sum_old:=sum;
        if (ADDer_count = 0) then
            sum_old := ((0),(0),(0),(0),(0),(0),(0),(0));
            sum := ((0),(0),(0),(0),(0),(0),(0),(0));
        end if;
      end if;
    end if;

  end process;

end Behavioral;
