-- Company: Fachhochschule Dortmund
-- Engineer: Javier Reyes
--
-- Create Date: 06/16/2017 04:49:58 PM
-- Design Name: Adder Testbench for Convolutional Codes example project
-- Module Name: MyADDER_tb - Simulation
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity MyADDER_tb is
  -- EMPTY
end MyADDER_tb;

architecture Behavioral of MyADDER_tb is

-- Signals declaration
signal INP1, INP2, INP3, INP4, INP5, INP6, INP7, INP8 : integer;
signal INP9, INP10, INP11, INP12, INP13, INP14, INP15, INP16 : integer;
signal selectx1, selectx2, selectx3, selectx4 :  bit;
signal selectx5, selectx6, selectx7, selectx8 :  bit;
signal clk : std_logic := '0';
signal count : integer := 0;
signal OUP1_1, OUP1_5, OUP5_3, OUP5_7, OUP3_2, OUP3_6, OUP7_4, OUP7_8 : integer;
signal OUP2_1, OUP2_5, OUP6_3, OUP6_7, OUP4_2, OUP4_6, OUP8_4, OUP8_8 : integer;

-- Components declaration
component MyADDER
  port (INP1, INP2, INP3, INP4, INP5, INP6, INP7, INP8 : in integer;
        INP9, INP10, INP11, INP12, INP13, INP14, INP15, INP16 : in integer;
        selectionIndex1, selectionIndex2, selectionIndex3, selectionIndex4 : in bit;
        selectionIndex5, selectionIndex6, selectionIndex7, selectionIndex8 : in bit;
        ADDer_clk : in std_logic;
        ADDer_count : in integer;
        OUP1_1, OUP1_5, OUP5_3, OUP5_7, OUP3_2, OUP3_6, OUP7_4, OUP7_8 : out integer;
        OUP2_1, OUP2_5, OUP6_3, OUP6_7, OUP4_2, OUP4_6, OUP8_4, OUP8_8 : out integer);
end component;

begin
  -- instantiating the components
  add1 : MyADDER port map(INP1, INP2, INP3, INP4, INP5, INP6, INP7, INP8,
                          INP9, INP10, INP11, INP12, INP13, INP14, INP15, INP16,
                          selectx1, selectx2, selectx3, selectx4,
                          selectx5, selectx6, selectx7, selectx8,
                          clk, count,
                          OUP1_1, OUP1_5, OUP5_3, OUP5_7, OUP3_2, OUP3_6, OUP7_4, OUP7_8,
                          OUP2_1, OUP2_5, OUP6_3, OUP6_7, OUP4_2, OUP4_6, OUP8_4, OUP8_8);

  -- Clock generated
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  -- Count generated
  count_process : process(clk)
  begin
    if (rising_edge(clk)) then
      count <= count + 1;
      if (count = 8) then
        count <= 0;
      end if;
    end if;
  end process;

  -- Stimulus generated
  d_in_process : process
  begin
    INP1 <= 0;
    INP2 <= 2;
    INP3 <= 2;
    INP4 <= 0;
    INP5 <= 1;
    INP6 <= 1;
    INP7 <= 1;
    INP8 <= 1;
    INP9 <= 2;
    INP10 <= 0;
    INP11 <= 0;
    INP12 <= 2;
    INP13 <= 1;
    INP14 <= 1;
    INP15 <= 1;
    INP16 <= 1; selectx1 <= '1';  selectx8 <= '0';
    wait for clk_period; --0
    INP1 <= 0;
    INP2 <= 2;
    INP3 <= 2;
    INP4 <= 0;
    INP5 <= 1;
    INP6 <= 1;
    INP7 <= 1;
    INP8 <= 1;
    INP9 <= 2;
    INP10 <= 0;
    INP11 <= 0;
    INP12 <= 2;
    INP13 <= 1;
    INP14 <= 1;
    INP15 <= 1;
    INP16 <= 1; selectx2 <= '1';  selectx1 <= '0';
    wait for clk_period; --1
    INP1 <= 1;
    INP2 <= 1;
    INP3 <= 1;
    INP4 <= 1;
    INP5 <= 0;
    INP6 <= 2;
    INP7 <= 2;
    INP8 <= 0;
    INP9 <= 1;
    INP10 <= 1;
    INP11 <= 1;
    INP12 <= 1;
    INP13 <= 2;
    INP14 <= 0;
    INP15 <= 0;
    INP16 <= 2; selectx3 <= '1';  selectx2 <= '0';
    wait for clk_period; --2
    INP1 <= 0;
    INP2 <= 2;
    INP3 <= 2;
    INP4 <= 0;
    INP5 <= 1;
    INP6 <= 1;
    INP7 <= 1;
    INP8 <= 1;
    INP9 <= 2;
    INP10 <= 0;
    INP11 <= 0;
    INP12 <= 2;
    INP13 <= 1;
    INP14 <= 1;
    INP15 <= 1;
    INP16 <= 1; selectx4 <= '1';  selectx3 <= '0';
    wait for clk_period; --3
    INP1 <= 1;
    INP2 <= 1;
    INP3 <= 1;
    INP4 <= 1;
    INP5 <= 0;
    INP6 <= 2;
    INP7 <= 2;
    INP8 <= 0;
    INP9 <= 1;
    INP10 <= 1;
    INP11 <= 1;
    INP12 <= 1;
    INP13 <= 2;
    INP14 <= 0;
    INP15 <= 0;
    INP16 <= 2; selectx5 <= '1';  selectx4 <= '0';
    wait for clk_period; --4
    INP1 <= 1;
    INP2 <= 1;
    INP3 <= 1;
    INP4 <= 1;
    INP5 <= 0;
    INP6 <= 2;
    INP7 <= 2;
    INP8 <= 0;
    INP9 <= 1;
    INP10 <= 1;
    INP11 <= 1;
    INP12 <= 1;
    INP13 <= 2;
    INP14 <= 0;
    INP15 <= 0;
    INP16 <= 2; selectx6 <= '1';  selectx5 <= '0';
    wait for clk_period; --5
    INP1 <= 0;
    INP2 <= 2;
    INP3 <= 2;
    INP4 <= 0;
    INP5 <= 1;
    INP6 <= 1;
    INP7 <= 1;
    INP8 <= 1;
    INP9 <= 2;
    INP10 <= 0;
    INP11 <= 0;
    INP12 <= 2;
    INP13 <= 1;
    INP14 <= 1;
    INP15 <= 1;
    INP16 <= 1; selectx7 <= '1';  selectx6 <= '0';
    wait for clk_period; --6
    INP1 <= 0;
    INP2 <= 2;
    INP3 <= 2;
    INP4 <= 0;
    INP5 <= 1;
    INP6 <= 1;
    INP7 <= 1;
    INP8 <= 1;
    INP9 <= 2;
    INP10 <= 0;
    INP11 <= 0;
    INP12 <= 2;
    INP13 <= 1;
    INP14 <= 1;
    INP15 <= 1;
    INP16 <= 1; selectx8 <= '1';  selectx7 <= '0';
    wait for clk_period; --7
  end process;
end Behavioral;


--T=0
--
