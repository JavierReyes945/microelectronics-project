-- Company: Fachhochschule Dortmund
-- Engineer: Mysara Ibrahim
--
-- Create Date: 27/06/2017 10:20:32 AM
-- Design Name: Top Implementation for Convolutional Codes example project
-- Module Name: top_implem - Behavioral
-- Project Name: Convolutional Codes example project

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.conf_pkg.all;

entity top_implem is
  -- EMPTY
end top_implem;

architecture Behavioral of top_implem is

-- Signals declaration
signal in_data    : std_logic_vector(k-1 downto 0) := std_logic_vector(to_unsigned(0, k));
signal clk        : std_logic := '0';
signal enc_rs     : std_logic := '0';
signal enc_data   : std_logic_vector(n-1 downto 0);
signal noise_data : std_logic_vector(n-1 downto 0);
signal dec_out    : std_logic_vector(6 downto 0);

-- Components declaration
component decoder
  port (EncIN   : in  std_logic_vector(n-1 downto 0);
        Dec_clk : in  std_logic;
        DecOUT  : out std_logic_vector(6 downto 0));
end component;

component encoder
port (enc_in  : in std_logic_vector(k-1 downto 0);
      enc_clk : in std_logic;
      enc_rs  : in std_logic;
      enc_out : out std_logic_vector(n-1 downto 0));
end component;

component noise
  port (nin  : in  std_logic_vector(n-1 downto 0);
        nout : out std_logic_vector(n-1 downto 0));
end component;

begin

  -- instantiating the components
  ENC    : encoder port map (in_data, clk, enc_rs, enc_data);
  Noise1 : noise   port map (enc_data, noise_data);
  DEC    : decoder port map (noise_data, clk, dec_out);

  -- Clock generated
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process;

  -- Stimulus generated
  stim_proc : process
  begin
    wait for clk_period/2;
    in_data <= std_logic_vector(to_unsigned(1, k));
    wait for clk_period;
    in_data <= std_logic_vector(to_unsigned(0, k));
    wait for clk_period;
    in_data <= std_logic_vector(to_unsigned(1, k));
    wait for clk_period;
    in_data <= std_logic_vector(to_unsigned(1, k));
    wait for clk_period;
    in_data <= std_logic_vector(to_unsigned(0, k));
    wait for clk_period;
    in_data <= std_logic_vector(to_unsigned(0, k));
    wait for clk_period;
    in_data <= std_logic_vector(to_unsigned(0, k));
    wait for clk_period/2;
    wait for clk_period*3;
  end process;

end Behavioral;
